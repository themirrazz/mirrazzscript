function preventFreezing(script) {
    // for older browsers
    function replaceAll(t,d,w) {
        var g=t
        while(d.constructor===RegExp?g.match(d):g.includes(d)){
            g=g.replace(d,w)
        }
        return g
    }
    // main
    return replaceAll(
        replaceAll(
            replaceAll(
                script,
                /(?<!async)\((?=[\u0000-\u9999]*\)=>)/,
                'async('
            ),
            /(?<!async )function(?=[\u0000-\u9999]*\(\)=>)/,
            'async function ',
            /(?<while\([\u0000-\u9999]*)\)\{/,
            '){\nawait new Promise(e=>setTimeout(()=>e(),1);\n'
        )
    )
}
